###################################################
#
# file: Makefile
#
# @Author:   Iacovos G. Kolokasis
# @Version:  10-02-2021
# @email:    kolokasis@csd.uoc.gr
#
# Makefile
#
####################################################

install:
	-pip3 install --upgrade python-gitlab --user

turnin:
	-git tag assignment1
	-git push origin --tags

undoTurnin:
	if git tag --list | egrep -q "assignment1"; then \
		git tag -d assignment1 && git push origin :refs/tags/assignment1; \
		fi
